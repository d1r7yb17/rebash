# rebash
Collection of re(-usable) bash scripts.

## Nextcloud
The **Nextcloud** scripts automate repetetive nextcloud maintenance tasks.
Use them in cronjobs for maximum profit.

## Testing
The **Testing** script are only for fun and education.
They shall be ignored and should never be used in production environmen.

## Utils
The **Utils** are little helpers that will make life easier.

